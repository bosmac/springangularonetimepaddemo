package com.marcelo.cryptography.logic;

import java.util.Random;

public class OneTimePadKey {

	private int[] key;
	private Random random;
	public final String ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public OneTimePadKey(){
		super();
		this.random = new Random();
	}
	
	public void generateRandomNumbers(int size){
		this.key = new int[size];
		for(int i=0; i<size; i++){
			this.key[i] = this.random.nextInt(this.ALPHABET.length());
		}
	}

	public int[] getKey() {
		return key;
	}

	public void setKey(int[] key) {
		this.key = key;
	}
	
	
}
