package com.marcelo.cryptography.logic;

public class OneTimePadCipher {
	
	public final String ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private String encryptedText;
	private String decryptedText;
	
	public OneTimePadCipher(){
		this.encryptedText = "";
		this.decryptedText = "";
	}

	public String getEncryptedText() {
		return encryptedText;
	}

	public void setEncryptedText(String encryptedText) {
		this.encryptedText = encryptedText;
	}

	public String getDecryptedText() {
		return decryptedText;
	}

	public void setDecryptedText(String decryptedText) {
		this.decryptedText = decryptedText;
	}

	public void encrypt(String decryptedText, int[] key)
	{
		this.decryptedText = decryptedText.toUpperCase();
		for(int i=0; i<this.decryptedText.length(); i++){			
			char decryptedChar = this.decryptedText.charAt(i);
			int decryptedCharIntValue = this.ALPHABET.indexOf(decryptedChar);
			int keyIntValue = key[i];
			int encryptedCharIntValue = Math.floorMod( (decryptedCharIntValue + keyIntValue) ,this.ALPHABET.length() );
			char encryptedChar = this.ALPHABET.charAt(encryptedCharIntValue);
			this.encryptedText = this.encryptedText + encryptedChar;
		}
	}
	
	public void decrypt(String encryptedText, int[] key)
	{
		this.encryptedText = encryptedText.toUpperCase();
		for(int i=0; i<this.encryptedText.length(); i++){
			char encryptedChar = this.encryptedText.charAt(i);
			int encryptedCharIntValue = this.ALPHABET.indexOf(encryptedChar);
			int keyIntValue = key[i];
			int decryptedCharIntValue = Math.floorMod( (encryptedCharIntValue - keyIntValue) ,this.ALPHABET.length() );
			char decryptedChar = this.ALPHABET.charAt(decryptedCharIntValue);
			this.decryptedText = this.decryptedText + decryptedChar;
		}
	}

}
