package com.marcelo.cryptography.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.marcelo.cryptography.logic.HelloWorld;
import com.marcelo.cryptography.logic.OneTimePadCipher;
import com.marcelo.cryptography.logic.OneTimePadKey;

@RestController
@CrossOrigin
@RequestMapping(value = "/onetimepad")
public class AppController {
	
	private OneTimePadKey otpk;
	
	public AppController(){
		this.otpk = new OneTimePadKey();
	}
	
	@RequestMapping(value = "/helloworld", method = { RequestMethod.GET, RequestMethod.POST })
	public HelloWorld helloWorld() {
		HelloWorld hw = new HelloWorld();
		hw.setGreeting("Hello World!");
		return hw;
	}
	
	@RequestMapping(value = "/encrypt", method = { RequestMethod.GET, RequestMethod.POST })
	public OneTimePadCipher encrypt(@RequestBody String input) {
		this.otpk.generateRandomNumbers(input.length());
		OneTimePadCipher otpc = new OneTimePadCipher();
		otpc.encrypt(input, otpk.getKey());
		return otpc;
	}
	
	@RequestMapping(value = "/decrypt", method = { RequestMethod.GET, RequestMethod.POST })
	public OneTimePadCipher decrypt(@RequestBody String input) {
		OneTimePadCipher otpc = new OneTimePadCipher();
		otpc.decrypt(input, otpk.getKey());
		return otpc;
	}
	
	@RequestMapping(value = "/encrypttest", method = { RequestMethod.GET, RequestMethod.POST })
	public OneTimePadCipher encrypttest(@RequestBody String input) {
		this.otpk.generateRandomNumbers(input.length());
		OneTimePadCipher otpc = new OneTimePadCipher();
		otpc.encrypt(input, otpk.getKey());
		return otpc;
	}
	
	@RequestMapping(value = "/decrypttest", method = { RequestMethod.GET, RequestMethod.POST })
	public OneTimePadCipher decrypttest(@RequestBody String input) {
		OneTimePadCipher otpc = new OneTimePadCipher();
		otpc.decrypt(input, otpk.getKey());
		return otpc;
	}


}